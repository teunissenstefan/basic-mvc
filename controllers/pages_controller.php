<?php
  class PagesController {
    public function home() {
      $first_name = 'Stefan';
      $last_name  = 'Teunissen';
      require_once('views/pages/home.php');
    }

    public function error() {
      require_once('views/pages/error.php');
    }
  }
?>